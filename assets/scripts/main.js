// Scroll Event Listener
try {
  const main_header = document.querySelector('.koel-main-header');
  document.addEventListener('scroll', e => {
    if (Math.floor(window.scrollY) < 50) {
      main_header.classList.remove('scrolled')
    } else {
      if (!main_header.classList.contains('scrolled')) {
        main_header.classList.add('scrolled');
      }
    }
  })
} catch (error) {
  console.log(error)
}

// Preview Image Control in page 1-3
try {
  const preview_image_show = document.querySelector("#preview-image-show");
  const preview_image_items = document.querySelectorAll('.preview-image-item');
  for (const preview_image of preview_image_items) {
    preview_image.addEventListener('click', e => {
      preview_image_show.setAttribute('src', preview_image.getAttribute('src'))
    })
  }
} catch (error) {
  console.log(error)
}

// Hamburger Mobile Control
try {
  const mobile_menu_opener = document.querySelector('#open-menu-full');
  const mobile_menu_closer = document.querySelector('#close-menu-full');
  const menu_full = document.querySelector('#menu-full');

  mobile_menu_opener.addEventListener('click', (e) => {
    e.preventDefault();
    menu_full.classList.toggle('koel-menu-full-active');
  })
  mobile_menu_closer.addEventListener('click', (e) => {
    e.preventDefault();
    menu_full.classList.toggle('koel-menu-full-active');
  })
} catch (error) {

}

// Masonry Layout Blog
try {
  function resizeGridItem(item) {
    grid = document.getElementsByClassName("grid")[0];
    rowHeight = parseInt(
      window.getComputedStyle(grid).getPropertyValue("grid-auto-rows")
    );
    rowGap = parseInt(
      window.getComputedStyle(grid).getPropertyValue("grid-row-gap")
    );
    rowSpan = Math.ceil(
      (item.querySelector(".content").getBoundingClientRect().height +
        rowGap) /
      (rowHeight + rowGap)
    );
    item.style.gridRowEnd = "span " + rowSpan;
  }

  function resizeAllGridItems() {
    allItems = document.getElementsByClassName("item");
    for (x = 0; x < allItems.length; x++) {
      resizeGridItem(allItems[x]);
    }
  }

  function resizeInstance(instance) {
    item = instance.elements[0];
    resizeGridItem(item);
  }

  window.onload = resizeAllGridItems();
  window.addEventListener("resize", resizeAllGridItems);

  allItems = document.getElementsByClassName("item");
  for (x = 0; x < allItems.length; x++) {
    imagesLoaded(allItems[x], resizeInstance);
  }
} catch (error) {
  console.log(error)
}
